import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavFruitsComponent } from './fav-fruits.component';

describe('FavFruitsComponent', () => {
  let component: FavFruitsComponent;
  let fixture: ComponentFixture<FavFruitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavFruitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavFruitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
