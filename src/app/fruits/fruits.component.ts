import { Component, OnInit } from '@angular/core';
import { FruitsService } from './fruit.service';

@Component({
  selector: 'app-fruits',
  templateUrl: './fruits.component.html',
  styleUrls: ['./fruits.component.css'],
  providers: [ FruitsService ]
})
export class FruitsComponent implements OnInit {
  fruits: string[];
  btnDisable = false;
  inputFruit = null;
  Adding = false;

  AddFruit(){
    this.fruits.push(this.inputFruit)
    this.Adding = false
  }

  AddMore(){
    this.Adding = !this.Adding
  }
  constructor(private ajax: FruitsService) {
    this.fruits = this.ajax.fruits
   }

  ngOnInit() {
  }

}
