import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from "@angular/forms";
import { AppComponent } from './app.component';
import { FruitsComponent } from './fruits/fruits.component';
import { FavFruitsComponent } from './fav-fruits/fav-fruits.component';
import { RouterModule, Routes } from '@angular/router';

const routeLists : Routes = [
  {path:"", component:FavFruitsComponent},
  {path:"fruits", component:FruitsComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    FruitsComponent,
    FavFruitsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routeLists)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
